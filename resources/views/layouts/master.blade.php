<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
         https://firebase.google.com/docs/web/setup#available-libraries -->
    {{--<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-analytics.js"></script>--}}

{{--<!-- The core Firebase JS SDK is always required and must be listed first -->--}}
    {{--<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-app.js"></script>--}}

    {{--<!-- TODO: Add SDKs for Firebase products that you want to use--}}
         {{--https://firebase.google.com/docs/web/setup#available-libraries -->--}}
    {{--<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-analytics.js"></script>--}}

    <link rel="manifest" href="{{asset('manifest.json')}}">
</head>
<body>

<script src="{{asset('js/firebase.js')}}"></script>
</body>
</html>