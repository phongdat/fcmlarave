importScripts('https://www.gstatic.com/firebasejs/3.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.0/firebase-messaging.js');

const firebaseConfig = {
    apiKey: "AIzaSyAAl8lA_qv-McfLstk8Vsqw79vWyo3koZA",
    authDomain: "fcmlaravel-2a8b4.firebaseapp.com",
    databaseURL: "https://fcmlaravel-2a8b4.firebaseio.com",
    projectId: "fcmlaravel-2a8b4",
    storageBucket: "testing-send-message",
    messagingSenderId: "593874509518",
    appId: "1:593874509518:web:24aa2cdb72162b7eda3a9f",
    measurementId: "G-ZSEQVHNFZX"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    const notificationTitle = 'Background Message from html';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});